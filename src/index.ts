#!/usr/bin/env node
import { asyncExists, asyncOpen, asyncClose, asyncWriteFile } from './utils';
import { Command, OptionValues } from 'commander';
import { IKfnFileReader, KfnData, KfnDirectoryFile } from './types';
import { KfnFileReader } from './KfnFileReader';
import { fileTypes } from './KfnUtils';

const pjson = require('../package.json');

const fileTypesHelperString: string = 
    Object.entries(Object.entries(fileTypes)
            .reduce((acc, pair) => { 
                if (acc[pair[1]] !== undefined) { 
                    acc[pair[1]].push(pair[0]) 
                } else { 
                    acc[pair[1]] = [pair[0]] 
                } 
                return acc;
            }, {}))
        .map(pair => `${pair[0]} (${pair[1]})`)
        .join(" | ");



export function readerForFile(fileDescriptor: number, fileName?: string): IKfnFileReader {
    return new KfnFileReader(fileDescriptor, fileName);
}

async function exportFile(kfnReader: KfnFileReader, file: KfnDirectoryFile, dir?: string) {
    if(!kfnReader.data.directory) 
        kfnReader.buildDirectory();
    const exportName = `${dir
        ? dir + '/'
        : kfnReader.filename.replace(/\.kfn$/, "") + '_'}${file.name}`;
    const buffer: Buffer = (await kfnReader.readFile(file.length1, file.offset));
    await asyncWriteFile(exportName, buffer);
    console.info(`Exported ${exportName}`)
    return exportName;
}

async function mainCLI(){
	const argv: OptionValues = parseArgs();
	if(!argv.in) 
		throw "--in argument is required";

    const kfnFile: string = argv.in;

	if(!await asyncExists(kfnFile))
		throw `File ${kfnFile} does not exist`;

    const fd: any = await asyncOpen(kfnFile, 'r');

    const kfnReader = new KfnFileReader(fd, kfnFile);

    await kfnReader.buildDirectory();
    const kfnData: KfnData = kfnReader.data;

    if(argv.export) {
        let fileFilter = (_: KfnDirectoryFile) => true;
        if(typeof argv.export !== "boolean") {
            if(isNaN(argv.export)) {
                const numType: number = fileTypes[argv.export];
                fileFilter = (file) => file.type === numType;
            } else {
                fileFilter = (file) => file.type === +argv.export;
            }
        }
        await Promise.all(kfnData.directory.files
            .filter(fileFilter)
            .map(file => exportFile(kfnReader, file, argv.directory)));
    }

    await asyncClose(fd);

    return argv.print ? JSON.stringify(kfnData) : "";
}

function parseArgs(): OptionValues {
	const argv = process.argv.filter(e => e !== '--');
    return new Command('kfn')
		.description('CLI for KFN file handling')
		.version(pjson.version)
		.option('-i, --in <kfnFile> (required)', 'the file to read data from')
		.option(`-e, --export [${fileTypesHelperString}]`, 'exports files of said type, defaults to all if no type is specified')
		.option('-d, --directory <dir>', 'where to export, defaults to same dir')
		.option('-p, --print', 'print file structure before ending')
        .parse(argv)
        .opts();
}


if (require.main === module) mainCLI()
	.then(data => console.log(data))
	.catch(err => console.error(err));