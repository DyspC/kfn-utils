
export interface KfnHeaders { [headerKey: string]: string | number }

export interface KfnDirectory {
    files: KfnDirectoryFile[],
    offset?: number,
}

export interface KfnDirectoryFile {
    name: string,
    type: number,
    length1: number, 
    offset: number, 
    length2: number, 
    flags: number,
}

export interface KfnData {
    headers?: KfnHeaders,
    directory?: KfnDirectory,
}

export interface IKfnFileReader {
    /**
     * Structured data describing the KFN file's content. 
     * Empty until {@link buildDirectory} is called.
     */
    data: KfnData;

    /**
     * Reads raw file data from the kfn file
     * Appropriate parameter values will be available in {@link data}
     * @param size the number of bytes
     * @param offset the starting offset
     */
    readFile(size: number, offset: number): Promise<Buffer>;

    /**
     * Loads file metadata
     */
    buildHeaders(): Promise<void>;

    /**
     * Loads directory index
     */
    buildDirectory(): Promise<void>;

}

export function readerForFile(fileDescriptor: number, fileName?: string): IKfnFileReader;