import { KfnDirectoryFile, KfnHeaders, KfnDirectory, KfnData } from "./types";
import { WriteStream, createWriteStream, existsSync, openSync, readSync, closeSync } from "fs";

const COPY_CHUNK_SIZE: number = 65536;

export class KfnFileWriter {

    fileWriteStream: WriteStream;

    constructor(public filePath: string, public sourceDir: string = ".", public sourcePrefix: string = "") {
        this.fileWriteStream = createWriteStream(filePath, {
            flags: 'w',
            encoding: 'binary',
        });
    }

    writeKfn(kfnData: KfnData) {
        this._writeSignature();
        this._writeHeader(kfnData.headers);
        this._writeDirectory(kfnData.directory);
        
        kfnData.directory.files.sort((file1, file2) => file1.offset - file2.offset).forEach(file => this._writeFile(file));

        this.fileWriteStream.close();
    }
    
    private _writeNumber(data: number, size: number = 1): void {
        const buffer: Buffer = Buffer.alloc(size, 0);
        buffer.writeIntLE(data, 0, size);
        this.fileWriteStream.write(buffer);
    }

    private _writeString(data: string): void {
        const buffer: Buffer = Buffer.from(data, 'utf8');
        this.fileWriteStream.write(buffer);
    }

    private _writeSizedString(data: string): void {
        const buffer: Buffer = Buffer.from(data, 'utf8');
        this._writeNumber(buffer.byteLength, 4);
        this.fileWriteStream.write(buffer);
    }

    private _writeFile(file: KfnDirectoryFile): void {
        const expandedPath: string = `${this.sourceDir}/${this.sourcePrefix}${file.name}`;
        if(!existsSync(expandedPath)) {
            throw `Missing file: ${expandedPath}`
        }
        let bytesRead: number = 0;
        let toRead: number = file.length1;
        const inputFd: number = openSync(expandedPath, 'r');
        const buffer: Buffer = Buffer.alloc(COPY_CHUNK_SIZE);
        while(toRead > 0 && 0 < (bytesRead = readSync(inputFd, buffer, 0, Math.min(toRead, COPY_CHUNK_SIZE), null))) {
            const chunk: Buffer = Buffer.from(buffer.subarray(0, bytesRead));
            this.fileWriteStream.write(chunk);
            toRead -= bytesRead;
        }
        closeSync(inputFd);
        if(toRead !== 0) {
            throw `Given file ${JSON.stringify(file)} had a different length (${toRead} bytes difference)`;
        }
    }

    private _writeSignature(): void {
        this._writeString("KFNB");
    }

    private _writeHeader(headers: KfnHeaders): void {
        Object.entries(headers)
            // .sort(([k, _]) => k === "ENDH" ? 1 : 0)
            .forEach(([key, value]) => {
                this._writeString(key);
                if(typeof value === "number") {
                    this._writeNumber(1, 1);
                    this._writeNumber(value, 4);
                } else {
                    this._writeNumber(2, 1);
                    this._writeSizedString(value);
                }
            });
    }

    private _writeDirectory(dir: KfnDirectory): void {
        this._writeNumber(dir.files.length, 4);
        dir.files.forEach(file => {
            this._writeSizedString(file.name);
            this._writeNumber(file.type, 4);
            this._writeNumber(file.length1, 4);
            this._writeNumber(file.offset, 4);
            this._writeNumber(file.length2, 4);
            this._writeNumber(file.flags, 4);
        });
    }

}

