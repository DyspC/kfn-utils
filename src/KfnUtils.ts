import { Stats, lstat } from "fs";
import { KfnDirectory, KfnDirectoryFile } from "./types";
import { basename } from "path";
import { promisify } from "util";
import { fromFile as parseMimeType } from 'file-type';
import { asyncReadFile } from "./utils";
import { MimeType } from "file-type/core";

export const fileTypes: {[label: string]: number} = {
    text: 1,
    lyrics: 1,
    music: 2, 
    audio: 2, 
    image: 3,
    font: 4, 
    video: 5,
}

export function inferFileType(mime: string): number {
    let type: number;
    const mimeP1: string = mime.split('/')[0];
    switch(mimeP1){
        case "text":
        case "audio":
        case "image":
        case "font":
        case "video":
            type = fileTypes[mimeP1];
            break;
        default: 
            throw `Unsupported MimeType: ${mime}`;
    }
    return type;
}


export async function addFileToDirectory(directory: KfnDirectory, filePath: string): Promise<void> {
    const stats: Stats = await promisify(lstat)(filePath);
    let mimeType: MimeType = (await parseMimeType(filePath))?.mime;
    if(mimeType === undefined){
        const fileContent: string = (await asyncReadFile(filePath)).toString('utf-8');
        if (/^(((;|#).*)|\s)*\[[A-Za-z0-9 _-]+\]\r?\n/.test(fileContent)) {
            // file-type won't detect text mimes so find a way to check if it's INI formatted data 
            mimeType = 'text/plain' as MimeType;
        } else {
            throw `Could not get mime type for file path ${filePath}. If it's a text file only INI format is supported.`;
        }
    }
    const rawEntry: Partial<KfnDirectoryFile> = {
        flags: 0,
        length1: stats.size,
        length2: stats.size,
        name: basename(filePath),
        type: inferFileType(mimeType),
    }
    directory.files = directory.files.sort((file1, file2) => file1.offset - file2.offset); // force order
    const oldFiles: KfnDirectoryFile[] = directory.files.filter(file => file.name === rawEntry.name);
    if(oldFiles.length > 0) { // replace and fix offsets
        directory.files = directory.files.filter(file => file.name !== rawEntry.name);
        rawEntry.offset = oldFiles[0].offset;
        directory.files.forEach(file => { 
            if(file.offset > rawEntry.offset) 
                file.offset += (rawEntry.length1-oldFiles[0].length1)
        });
    } else { // add before Song.ini
        const lastFile: KfnDirectoryFile = directory.files[directory.files.length-1];
        rawEntry.offset = lastFile.offset;
        lastFile.offset += rawEntry.length1;
    }
    directory.files.push(rawEntry as KfnDirectoryFile);
}