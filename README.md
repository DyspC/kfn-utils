# kfn-utils

Command line utils to manage KFN files

Based on [this reverse engineering](https://www.ulduzsoft.com/2012/10/reverse-engineering-the-karafun-file-format-part-1-the-header/)

## options

 - `-i <file>` input file
 - `-e [type]` export directory (filtered)
 - `-d <dir>` where to export
 - `-p` print structure before ending
 - `-h` better help page
