
import { Command, OptionValues } from 'commander';
import { asyncClose, asyncExists, asyncOpen } from '../src/utils'
import { KfnData } from '../src/types'
import { KfnFileReader } from '../src/KfnFileReader'
import { addFileToDirectory } from '../src/KfnUtils'



async function mainCLI(){
	const argv: OptionValues = parseArgs();
	if(!argv.in) 
		throw "--in argument is required";

    const kfnFile: string = argv.in;

	if(!await asyncExists(kfnFile))
		throw `File ${kfnFile} does not exist`;

    const fd: any = await asyncOpen(kfnFile, 'r');

    const kfnReader = new KfnFileReader(fd, kfnFile);

    await kfnReader.buildDirectory();
    const kfnData: KfnData = kfnReader.data;

    console.debug(`File structure before adding a file: ${JSON.stringify(kfnData.directory, null, 2)}`);
    await addFileToDirectory(kfnData.directory, argv.add);
    console.debug(`File structure after adding a file: ${JSON.stringify(kfnData.directory, null, 2)}`);

    await asyncClose(fd);

    return argv.print ? JSON.stringify(kfnData) : "";
}

function parseArgs(): OptionValues {
	const argv = process.argv.filter(e => e !== '--');
    return new Command('kfn-test-writer')
        .option('-i, --in <kfnFile> (required)', 'the file to edit directory from')
        .option('-a, --add <file> (required)', 'the file to add')
        .parse(argv)
        .opts();
}


if (require.main === module) mainCLI()
	.then(data => console.log(data))
	.catch(err => console.error(err));